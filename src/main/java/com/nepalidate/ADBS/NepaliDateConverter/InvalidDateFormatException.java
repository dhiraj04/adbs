package com.nepalidate.ADBS.NepaliDateConverter;

public class InvalidDateFormatException extends RuntimeException {

    public InvalidDateFormatException(String message) {
        super(message);
    }
}
