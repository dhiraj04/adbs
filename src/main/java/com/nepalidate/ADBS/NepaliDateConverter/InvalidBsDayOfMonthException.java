package com.nepalidate.ADBS.NepaliDateConverter;

public class InvalidBsDayOfMonthException extends RuntimeException {

    public InvalidBsDayOfMonthException(String message) {
        super(message);
    }
}
