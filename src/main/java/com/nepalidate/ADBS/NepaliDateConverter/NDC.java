package com.nepalidate.ADBS.NepaliDateConverter;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

@Component
public class NDC {

    public String adToBs(String adDate){

        Pattern datePattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");

        if(datePattern.matcher(adDate).matches()){

            String[] arrayOfStr = adDate.split("-");
            String year = arrayOfStr[0];
            String month = arrayOfStr[1];
            String day = arrayOfStr[2];

            AdBs dc = new AdBs();
            String bsDate = null;

            try {
                String adDate_formatted = day+"-"+month+"-"+year;
                bsDate = dc.convertAdToBs(adDate_formatted);
            } catch (ParseException e){
                System.out.println("ParseException = " + e);
            }

            assert bsDate != null;

            String[] arrayOfBsDate = bsDate.split("-");
            String year_bs = arrayOfBsDate[0];
            String month_bs = arrayOfBsDate[1];
            String day_bs = arrayOfBsDate[2];

            if(Integer.parseInt(month_bs) <10 && Integer.parseInt(month_bs) >= 1){
                month_bs = String.format("%02d", Integer.parseInt(month_bs));
            }

            if(Integer.parseInt(day_bs) <10 && Integer.parseInt(day_bs) >= 1){
                day_bs = String.format("%02d", Integer.parseInt(day_bs));
            }

            return year_bs+"/"+month_bs+"/"+day_bs;

        } else {
            throw  new InvalidDateFormatException("Invalid Date Format, please use yyyy-MM-dd format");
        }
    }

    public String bsToAd(String bsDate){

        Pattern datePattern = Pattern.compile("\\d{4}/\\d{2}/\\d{2}");
        if(datePattern.matcher(bsDate).matches()){

            String[] arrayOfStr = bsDate.split("/");
            String yearBs = arrayOfStr[0];
            String monthBs = arrayOfStr[1];
            String dayBs = arrayOfStr[2];

            int year_bs_int = Integer.parseInt(yearBs);
            int month_bs_int = Integer.parseInt(monthBs);
            int day_bs_int = Integer.parseInt(dayBs);

            if(Integer.parseInt(monthBs) > 12 || Integer.parseInt(monthBs) < 1){
                throw new DateRangeNotSupported("Invalid Bs Month = " + monthBs);
            }

            AdBs dc = new AdBs();
            Date adDate = null;

            if(dc.validateBsDate(year_bs_int, month_bs_int, day_bs_int)){
                String bsDate_formatted = dayBs+monthBs+yearBs;
                adDate = dc.convertBsToAd(bsDate_formatted);
            } else {
                throw new IllegalStateException("invalid BS date");
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.format(adDate);

        } else {
            throw  new InvalidDateFormatException("Invalid Date Format, please use yyyy/MM/dd format");
        }
    }

    public boolean validateDate_bs(String bsDate, String field_name){

        Pattern datePattern = Pattern.compile("\\d{4}/\\d{2}/\\d{2}");
        if(datePattern.matcher(bsDate).matches()){

            String[] arrayOfStr = bsDate.split("/");
            String y_bs = arrayOfStr[0];
            String m_bs = arrayOfStr[1];
            String d_bs = arrayOfStr[2];

            int yearBs = Integer.parseInt(y_bs);
            int monthBs = Integer.parseInt(m_bs);
            int dayBs = Integer.parseInt(d_bs);

            if(monthBs > 12 || monthBs < 1){
                throw new DateRangeNotSupported("Invalid Bs Month = " + monthBs + " for " + field_name);
            }

            int dayOfMonth = Lookup.numberOfDaysInNepaliMonth.get(yearBs)[monthBs -1];
            if(dayBs <= dayOfMonth) {
                return true;
            } else {
                String message = String.format("invalid day of month  %d for year  %d  and month  %d for field %s", dayBs, yearBs, monthBs, field_name);
                throw new InvalidBsDayOfMonthException(
                        message);
            }

        } else {
            throw  new InvalidDateFormatException(field_name + " Invalid Date Format, please use yyyy/MM/dd format");
        }
    }
}
