package com.nepalidate.ADBS.NepaliDateConverter;

public class DateRangeNotSupported extends RuntimeException {

    public DateRangeNotSupported(String message) {
        super(message);
    }
}
