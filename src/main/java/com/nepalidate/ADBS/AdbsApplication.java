package com.nepalidate.ADBS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdbsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdbsApplication.class, args);
	}

}
